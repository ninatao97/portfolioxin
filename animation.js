

/*===============FULLPAGE====================*/
new fullpage("#fullPage" , {
    autoScrolling: true,
    navigation: true,
    onLeave:(origin, destination, direction) => {
    if(destination.index === 1){
        const title = document.querySelector('#title');
        const description1 = document.querySelector('#description1');
        const description2 = document.querySelector('#description2');
        const description3 = document.querySelector('#description3');
        const description4 = document.querySelector('#description4');
        const td = new TimelineMax({ delay: 1});
        td.fromTo(title, 0.7, { x:"-100", opacity: 0},{ x:0, opacity: 1});
        td.fromTo(description1, 0.5, { y:"-100", opacity: 0},{ y:0, opacity: 1});
        td.fromTo(description2, 0.5, { y:"-100", opacity: 0},{ y:0, opacity: 1});
        td.fromTo(description3, 0.5, { y:"-100", opacity: 0},{ y:0, opacity: 1});
        td.fromTo(description4, 0.5, { y:"-100", opacity: 0},{ y:0, opacity: 1});
        
       }
    }
});


let progress = 0;

function run(){
    if (progress < 1) {
        progress = parseFloat((progress + 0.01).toFixed(2))
        document.querySelector('h1').style.setProperty('--progress',progress)
        
        setTimeout(run,20)

    }
}

document.querySelector('#empty').addEventListener('click',(e)=>{
    progress = 0;
    document.querySelector('h1').style.setProperty('--progress',progress)
})
document.querySelector('#full').addEventListener('click',(e)=>{
    run();
})



/*=====================SWIPER==========================*/
var swiper = new Swiper('.swiper-container', {
    slidesPerView: 3,
    centeredSlides: true,
    spaceBetween: 20,
    /*grabCursor: true,*/
    pagination: {
      /*el: '.swiper-pagination',*/
      clickable: true,
    },
  });

 

/*==========================VANILLA=============================*/

VanillaTilt.init(document.querySelectorAll(".box"), {
    max: 25,
    speed: 400
});